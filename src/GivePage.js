import React, { useState, useEffect } from "react";
import get from "lodash.get";
import axios from "axios";
import PhoneInput from "react-phone-number-input";
import distance from "gps-distance";
import { BASE_URL } from "./constants";
import { ROUTES } from "./routes";
import { useHistory } from "react-router-dom";
import convert from "convert-units";
import LocationSelection from "./LocationSelection";
import styles from "./GivePage.module.scss";
import { Table, Button, Modal, Input } from "antd";

const USE_SAMPLE = process.env.REACT_APP_USE_SAMPLE_NEEDS;

const SAMPLE_NEEDS = [
  {
    key: "1",
    name: "Mike",
    needCategory: "Food",
    age: "70M",
    distance: "10 miles",
    needDescription:
      "Need someone to do grocery shopping for me. I'm over 65 so trying to stay inside if at all possible."
  },
  {
    key: "1",
    name: "Julianna",
    needCategory: "Social",
    age: "68F",
    distance: "1 mile",
    needDescription:
      "Just a chat, can be a call or video / facetime. I'm available 9am-7pm.  Immuno-compromised and under strict quarantine, just looking for someone to talk to for emotional support."
  }
];

const GivePage = props => {
  const [needs, setNeeds] = useState([]);
  const [name, setName] = useState([]);
  const [userLocation, setUserLocation] = useState();
  const [phoneNumber, setPhoneNumber] = useState();
  const [helpRecord, setHelpRecord] = useState();
  const history = useHistory();

  useEffect(() => {
    const fetchData = async () => {
      if (userLocation.longitude)
        // TODO add pagination
        axios
          .get(`${BASE_URL}/get-needs`, {
            params: userLocation
          })
          .then(response =>
            setNeeds(
              get(response, "data", []).map(need => ({
                ...need,
                key: need._id
              }))
            )
          )
          .catch(error => {
            console.error(error);
          });
    };
    if (userLocation) fetchData();
  }, [userLocation]);

  const fufillNeed = id => {
    if (!phoneNumber) {
      alert("please enter your phone number!");
      return;
    }
    if (!name) {
      alert("please enter your name!");
      return;
    }
    axios
      .get(`${BASE_URL}/fufill-need`, {
        params: {
          id,
          phoneNumber,
          name
        }
      })
      .then(response => {
        console.log(response);
        history.push({ pathname: ROUTES.SUBMISSION_SUCCESS });
      })
      .catch(error => console.error(error));
  };

  const columns = [
    {
      title: "Help",
      key: "help",
      render: (text, record) => (
        <span>
          <Button
            type="primary"
            style={{ marginRight: 16 }}
            onClick={() => setHelpRecord(record)}
          >
            Help {record.name}
          </Button>
        </span>
      )
    },
    {
      title: "Who",
      dataIndex: "name",
      key: "name"
    },
    {
      title: "Age",
      dataIndex: "age",
      key: "age",
      render: (text, record) => {
        const { age, gender } = record;
        if (!age && !gender) return "";
        let genderAbbreviation = "";
        switch (gender) {
          case "female": {
            genderAbbreviation = "F";
            break;
          }
          case "male": {
            genderAbbreviation = "M";
            break;
          }
          default:
            genderAbbreviation = "";
        }
        return `${age || ""}${genderAbbreviation}`;
      }
    },
    {
      title: "Need",
      dataIndex: "needCategory",
      key: "needCategory"
    },
    {
      title: "Why",
      dataIndex: "needDescription",
      key: "needDescription"
    },
    {
      title: "Distance",
      dataIndex: "distance",
      render: (text, record) => {
        if (typeof text === "string") return text;
        const needCoordiantes = get(record, "location.coordinates", []);
        const [needLong, needLat] = needCoordiantes;
        if (!userLocation) return "Loading...";
        const { latitude: userLat, longitude: userLong } = userLocation;
        if (!userLat || !userLong) return "Loading...";
        const milesAway = convert(
          distance(userLat, userLong, needLat, needLong)
        )
          .from("km")
          .to("mi")
          .toFixed(1);
        return `${milesAway} miles away`;
      }
    }
  ];

  const giveModal = (
    <Modal
      title={`Help ${get(helpRecord, "name")}`}
      visible={!!helpRecord}
      onOk={() => fufillNeed(helpRecord.key)}
      onCancel={() => setHelpRecord(null)}
    >
      <Input
        onChange={value => setName(value)}
        placeholder="Please enter your first name"
      />
      <PhoneInput
        defaultCountry="US"
        value={phoneNumber}
        onChange={setPhoneNumber}
      />
    </Modal>
  );

  return (
    <div className={styles["page"]}>
      <p>Please drag the pin to a location close to you to see results</p>
      <LocationSelection setLocation={setUserLocation} />
      <Table dataSource={USE_SAMPLE ? SAMPLE_NEEDS : needs} columns={columns} />
      {giveModal}
    </div>
  );
};

export default GivePage;

export const GiveSuccess = () => <h1>Thanks for giving</h1>; // TODO improve design
