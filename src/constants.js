const useLocal = process.env.NODE_ENV !== "production";

export const BASE_URL = useLocal
  ? `http://localhost:9000`
  : `https://covid-mutual-aid-backend.herokuapp.com`;
