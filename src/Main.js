import React from "react";
import { Link } from "react-router-dom";
import { ROUTES } from "./routes";
import styles from "./Main.module.scss";

const Main = () => {
  return (
    <div className={styles["page"]}>
      <Link to={ROUTES.NEED} className={styles["need-button"]}>
        <span className={styles["text"]}>I have a need</span>
      </Link>
      <Link to={ROUTES.GIVE} className={styles["give-button"]}>
        <span className={styles["text"]}>I want to give</span>
      </Link>
    </div>
  );
};

export default Main;
