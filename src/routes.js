export const ROUTES = {
  MAIN: "/",
  NEED: "/need",
  GIVE: "/give",
  SUBMISSION_SUCCESS: "/submission-success",
  GIVE_SUCCESS: "/give-success"
};
